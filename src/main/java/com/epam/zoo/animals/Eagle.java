package com.epam.zoo.animals;

import com.epam.zoo.foods.Food;
import com.epam.zoo.foods.Meat;

import java.time.LocalTime;

public class Eagle extends Bird {

    public Eagle() {
        super(LocalTime.of(6, 0), LocalTime.of(23, 0));
    }

    public void eat(Meat food) {
        super.eat(food);
    }

    @Override
    public void eat(Food food) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String voice() {
        return "Eeeeek";
    }
}
