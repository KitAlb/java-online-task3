package com.epam.zoo.animals;

import com.epam.zoo.foods.Food;

import java.time.LocalTime;

public abstract class Fish extends Animal {

    Fish(LocalTime wakeUpTime, LocalTime sleepTime) {
        super(wakeUpTime, sleepTime);
    }

    @Override
    public String voice() {
        return "Ooo";
    }

    public void Swim() {}
}
