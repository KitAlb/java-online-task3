package com.epam.zoo.animals;

import com.epam.zoo.foods.Food;
import com.epam.zoo.foods.Meat;

import java.time.LocalTime;

public class Lion extends Animal {

    public Lion() {
        super(LocalTime.of(8, 0), LocalTime.of(21, 0));
    }

    public void eat(Meat food) {
        super.eat(food);
    }

    @Override
    public void eat(Food food) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String voice() {
        return "Rrrrrrr";
    }
}