package com.epam.zoo.animals;

import com.epam.zoo.foods.Food;

import java.time.LocalTime;

public abstract class Animal {

    private LocalTime wakeUpTime;
    private LocalTime sleepTime;

    private Animal() {

    }

    Animal(LocalTime wakeUpTime, LocalTime sleepTime) {
        this.wakeUpTime = wakeUpTime;
        this.sleepTime = sleepTime;
    }

    public boolean isSleep() {
        LocalTime now = LocalTime.now();
        if (sleepTime.isAfter(wakeUpTime)) {
            return !now.isBefore(sleepTime) || !now.isAfter(wakeUpTime);
        } else {
            return !now.isBefore(sleepTime) && !now.isAfter(wakeUpTime);
        }
    }

    public void eat(Food food) {

    }

    public String voice() {
        return "";
    }

}
