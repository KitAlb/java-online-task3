package com.epam.zoo.animals;

import com.epam.zoo.foods.Food;

import java.time.LocalTime;

public abstract class Bird extends Animal{
    Bird(LocalTime wakeUpTime, LocalTime sleepTime) {
        super(wakeUpTime, sleepTime);
    }

    @Override
    public void eat(Food food) {
        super.eat(food);
    }

    @Override
    public String voice() {
        return super.voice();
    }

    public void Fly() {

    }
}
