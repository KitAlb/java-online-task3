package com.epam.zoo.animals;

import com.epam.zoo.foods.Food;
import com.epam.zoo.foods.Meat;

import java.time.LocalTime;

public class Piranha extends Fish {

    public Piranha() {
        super(LocalTime.of(1, 0), LocalTime.of(10, 0));
    }

    @Override
    public void eat(Food food) {
        throw new UnsupportedOperationException();
    }

    public void eat(Meat food) {
        super.eat(food);
    }
}
