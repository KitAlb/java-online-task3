package com.epam.zoo.animals;

import com.epam.zoo.foods.Food;
import com.epam.zoo.foods.Plant;

import java.time.LocalTime;

public class Parrot extends Bird {

    public Parrot() {
        super(LocalTime.of(8, 0), LocalTime.of(20, 0));
    }

    public void eat(Plant food) {
        super.eat(food);
    }

    @Override
    public void eat(Food food) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String voice() {
        return "Hello!";
    }
}
