package com.epam.zoo;

import com.epam.zoo.animals.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Zoo {

    private static List<String> animalTypes = List.of(
            "eagle",
            "parrot",
            "piranha",
            "lion"
    );
    private List<Animal> animals = new ArrayList<>();

    private Zoo() {
    }

    public Zoo(int numberOfAnimals) {
        for (int i = 0; i < numberOfAnimals; i++) {
            animals.add(createRandomAnimal());
        }
    }

    public void run() {
        for (Animal animal : animals) {
            if (!animal.isSleep()) {
                System.out.println(animal.getClass().getSimpleName() + " says " + animal.voice());
            } else {
                System.out.println(animal.getClass().getSimpleName() + " is sleeping now.");
            }
        }
    }

    private Animal createRandomAnimal() {
        String animalName = animalTypes.get(new Random().nextInt(animalTypes.size()));

        if (animalName.equalsIgnoreCase("parrot")) {
            return new Parrot();
        } else if (animalName.equalsIgnoreCase("eagle")) {
            return new Eagle();
        } else if (animalName.equalsIgnoreCase("piranha")) {
            return new Piranha();
        } else if (animalName.equalsIgnoreCase("lion")) {
            return new Lion();
        }

        return new Eagle();
    }
}
