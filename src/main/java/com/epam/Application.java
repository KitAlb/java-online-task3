package com.epam;

import com.epam.shapes.*;
import com.epam.zoo.Zoo;

import java.util.List;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("----------- Start Zoo ----------");
        testZoo();
        System.out.println("----------- End Zoo ----------");
        System.out.println("");
        System.out.println("----------- Start Shapes ----------");
        testShapes();
        System.out.println("----------- End Shapes ----------");
    }

    private static void testZoo() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input number of animals:");
        int numberOfAnimals = scanner.nextInt();

        Zoo myZoo = new Zoo(numberOfAnimals);
        myZoo.run();
    }

    private static void testShapes() {
        List<Shape> shapes = List.of(
                new Rectangle(10, 20, "blue"),
                new Circle(15, "red"),
                new Triangle(3,4,5, "green")
        );
        for (Shape item: shapes){
            System.out.println(item.getClass().getSimpleName()+" area = "+ item.getArea());
            System.out.println(item.getClass().getSimpleName()+" perimeter = "+ item.getPerimeter());
            System.out.println(item.getClass().getSimpleName()+" color = "+ item.getColor());
        }
    }

}
