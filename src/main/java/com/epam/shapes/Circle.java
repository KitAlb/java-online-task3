package com.epam.shapes;

public class Circle extends Shape {
    private final double pi = 3.14;
    private int r;

    private Circle(String color) {
        super(color);
    }

    public Circle(int r, String color) {
        this(color);
        this.r = r;
    }

    private double getPerimeter(int r) {
        return 2 * pi * r;
    }

    private double getArea(int r) {
        return pi * (r * r);
    }

    @Override
    public double getPerimeter() {
        return getPerimeter(r);
    }

    @Override
    public double getArea() {
        return getArea(r);
    }
}