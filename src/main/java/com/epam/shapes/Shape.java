package com.epam.shapes;

public abstract class Shape {

    private String color;

    public Shape(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    abstract public double getPerimeter();
    abstract public double getArea();
}
