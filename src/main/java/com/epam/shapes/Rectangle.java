package com.epam.shapes;

public class Rectangle extends Shape {

    private int a, b;

    private Rectangle(String color) {
        super(color);
    }

    public Rectangle(int a, int b, String color) {
        this(color);
        this.a = a;
        this.b = b;
    }

    private double getPerimeter(int a, int b) {
        return 2 * (a + b);
    }

    private double getArea(int a, int b) {
        return a * b;
    }

    @Override
    public double getPerimeter() {
        return getPerimeter(a,b);
    }

    @Override
    public double getArea() {
        return getArea(a,b);
    }
}
