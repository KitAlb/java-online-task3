package com.epam.shapes;

public class Triangle extends Shape {
    private int a, b, c;

    private Triangle(String color) {
        super(color);
    }

    public Triangle(int a, int b, int c, String color) {
        this(color);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private double getPerimeter(int a, int b, int c) {
        return a + b + c;
    }

    private double getArea(int a, int b, int c) {
        return (a*b*Math.sin(c))/2;
    }

    @Override
    public double getPerimeter() {
        return getPerimeter(a,b,c);
    }

    @Override
    public double getArea() {
        return getArea(a,b,c);
    }
}
